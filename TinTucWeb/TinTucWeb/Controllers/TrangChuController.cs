﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TinTucWeb.Controllers
{
    public class TrangChuController : Controller
    {
        public ActionResult TrangChu()
        {
            ViewBag.ViewTrangChu = "Trang Chủ";
            ViewBag.ViewTheGioi = "<h1>Thế Giới</h1>";
            //ViewBag.ViewXaHoi = "Xã Hội";
            //ViewBag.ViewVanHoa = "Văn Hóa";
            //ViewBag.ViewKinhTe = "Kinh Tế       ";
            //ViewBag.ViewKHCN = "Khoa Học - Công Nghệ        ";
            //ViewBag.ViewTheThao = "Thể Thao     ";
            //ViewBag.ViewGiaiTri = "Giải Trí     ";
            //ViewBag.ViewPhapLuat = "Pháp Luật       ";
            //ViewBag.ViewGiaoDuc = "Giáo Dục     ";
            //ViewBag.ViewSucKhoe = "Sức Khỏe     ";
            return View();
        }
        public ActionResult TheGioi()
        {
            ViewBag.ViewTheGioi = "The Gioi";
            return View();
        }
        public ActionResult XaHoi()
        {
            ViewBag.ViewTheGioi = "Xã Hội";
            return View();
        }
        public ActionResult VanHoa()
        {
            ViewBag.ViewTheGioi = "Văn Hóa";
            return View();

        }
        public ActionResult KinhTe()
        {
            ViewBag.ViewTheGioi = "Kinh Tế";
            return View();
        }
        public ActionResult KHCN()
        {
            ViewBag.ViewTheGioi = "Khoa Học - Công Nghệ";
            return View();
        }
        public ActionResult TheThao()
        {
            ViewBag.ViewTheGioi = "Thể Thao";
            return View();
        }
        public ActionResult GiaiTri()
        {
            ViewBag.ViewTheGioi = "Giải Trí";
            return View();
        }
        public ActionResult PhapLuat()
        {
            ViewBag.ViewTheGioi = "Pháp Luật";
            return View();
        }
        public ActionResult GiaoDuc()
        {
            ViewBag.ViewTheGioi = "Giáo Dục";
            return View();
        }
        public ActionResult SucKhoe()
        {
            ViewBag.ViewTheGioi = "Sức Khỏe";
            return View();
        }

    }
}
