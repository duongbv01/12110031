﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TinTucWeb.Models
{
    public class SuKien
    {
        public SuKien()
        {
            this.Accounts = new HashSet<Account>();
        }
        [Key]
        public int idSuKien { get; set; }
        public string moTa { get; set; }

        public virtual ICollection<Account> Accounts { get; set; }
    }
}