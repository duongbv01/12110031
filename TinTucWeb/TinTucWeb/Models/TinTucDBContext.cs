﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TinTucWeb.Models
{
    public class TinTucDBContext : DbContext
    {
        public DbSet<TheLoai> TheLoais { set; get; }
        public DbSet<Tin> Tins { set; get; }
        public DbSet<Account> Accounts { set; get; }
        public DbSet<CauHoiTinTuc> CauHoiTinTucs { set; get; }
        public DbSet<LoaiTin> LoaiTins { set; get; }
        public DbSet<NhanXetBanDoc> NhanXetBanDocs { set; get; }
        public DbSet<PhanThuong> PhanThuongs { set; get; }
        public DbSet<QuyenThamGiaTroChoiTrongNgay> QuyenThamGiaTroChoiTrongNgays { set; get; }
        public DbSet<SuKien> SuKiens { set; get; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Tin>()
                .HasKey(t => t.idTin)
                .HasMany(d => d.Accounts).WithMany(d => d.Tins)
                .Map(t => t.MapLeftKey("idTin").MapRightKey("idAccount").ToTable("Account_Tin"));
        }
    }
}