﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog5.Models
{
    public class Comment
    {
        public int ID { set; get; }
       
        public string Body { set; get; }
        public DateTime DateCreate { set; get; }
        public int lastTime
        {
            get
            {
                return (DateTime.Now - DateCreate).Minutes;
            }
        }
       
        public int PostID { set; get; }
        public virtual Post post { set; get; }
    }
}