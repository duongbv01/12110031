﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TinTucWeb.Models;

namespace TinTucWeb.Controllers
{
    public class NhanXetBanDocController : Controller
    {
        private TinTucDBContext db = new TinTucDBContext();

        //
        // GET: /NhanXetBanDoc/

        public ActionResult Index()
        {
            var nhanxetbandocs = db.NhanXetBanDocs.Include(n => n.Tin);
            return View(nhanxetbandocs.ToList());
        }

        //
        // GET: /NhanXetBanDoc/Details/5

        public ActionResult Details(int id = 0)
        {
            NhanXetBanDoc nhanxetbandoc = db.NhanXetBanDocs.Find(id);
            if (nhanxetbandoc == null)
            {
                return HttpNotFound();
            }
            return View(nhanxetbandoc);
        }

        //
        // GET: /NhanXetBanDoc/Create

        public ActionResult Create()
        {
            ViewBag.idTin = new SelectList(db.Tins, "idTin", "tieuDe");
            return View();
        }

        //
        // POST: /NhanXetBanDoc/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NhanXetBanDoc nhanxetbandoc)
        {
            if (ModelState.IsValid)
            {
                db.NhanXetBanDocs.Add(nhanxetbandoc);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idTin = new SelectList(db.Tins, "idTin", "tieuDe", nhanxetbandoc.idTin);
            return View(nhanxetbandoc);
        }

        //
        // GET: /NhanXetBanDoc/Edit/5

        public ActionResult Edit(int id = 0)
        {
            NhanXetBanDoc nhanxetbandoc = db.NhanXetBanDocs.Find(id);
            if (nhanxetbandoc == null)
            {
                return HttpNotFound();
            }
            ViewBag.idTin = new SelectList(db.Tins, "idTin", "tieuDe", nhanxetbandoc.idTin);
            return View(nhanxetbandoc);
        }

        //
        // POST: /NhanXetBanDoc/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(NhanXetBanDoc nhanxetbandoc)
        {
            if (ModelState.IsValid)
            {
                db.Entry(nhanxetbandoc).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idTin = new SelectList(db.Tins, "idTin", "tieuDe", nhanxetbandoc.idTin);
            return View(nhanxetbandoc);
        }

        //
        // GET: /NhanXetBanDoc/Delete/5

        public ActionResult Delete(int id = 0)
        {
            NhanXetBanDoc nhanxetbandoc = db.NhanXetBanDocs.Find(id);
            if (nhanxetbandoc == null)
            {
                return HttpNotFound();
            }
            return View(nhanxetbandoc);
        }

        //
        // POST: /NhanXetBanDoc/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            NhanXetBanDoc nhanxetbandoc = db.NhanXetBanDocs.Find(id);
            db.NhanXetBanDocs.Remove(nhanxetbandoc);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}