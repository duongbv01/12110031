﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TinTucWeb.Models
{
    public class QuyenThamGiaTroChoiTrongNgay
    {
        [Key]
        public int idAccount { get; set; }
        public bool duocTraLoi { get; set; }
        public System.DateTime ngayThamGia { get; set; }
        public Nullable<int> Account_idAccount { get; set; }

        public virtual Account Account { get; set; }
    }
}