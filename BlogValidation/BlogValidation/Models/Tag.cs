﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BlogValidation.Models
{
    public class Tag
    {
        public int ID { set; get; }
        [StringLength(250, ErrorMessage = "So ky tu trong khoang 5-250", MinimumLength = 5)]
        public string Content { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}