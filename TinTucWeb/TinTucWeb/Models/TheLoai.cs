﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TinTucWeb.Models
{
    public class TheLoai
    {
        public TheLoai()
        {
            this.Tins = new HashSet<Tin>();
        }
        [Key]
        public int idTheLoai { get; set; }
        public string tenTheLoai { get; set; }
        public int anHien { get; set; }

        public virtual ICollection<Tin> Tins { get; set; }
    }
}