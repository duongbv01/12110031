﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TinTucWeb.Models
{
    public class CauHoiTinTuc
    {
        [Key]
        public int idCauHoi { get; set; }
        public string noiDungCH { get; set; }
        public string dapAnCH { get; set; }
        public System.DateTime ngayCapNhatCH { get; set; }
        public Nullable<int> Account_idAccount { get; set; }

        public virtual Account Account { get; set; }
    }
}