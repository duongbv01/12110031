﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TinTucWeb.Models
{
    public class Account
    {
        public Account()
        {
            this.CauHoiTinTucs = new HashSet<CauHoiTinTuc>();
            this.NhanXetBanDocs = new HashSet<NhanXetBanDoc>();
            this.QuyenThamGiaTroChoiTrongNgays = new HashSet<QuyenThamGiaTroChoiTrongNgay>();
            this.Tins = new HashSet<Tin>();
        }
        [Key]
        public int idAccount { get; set; }
        public string hoTen { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public string diaChi { get; set; }
        public string dienThoai { get; set; }
        public string email { get; set; }
        public System.DateTime ngayDangKy { get; set; }
        public string group { get; set; }
        public System.DateTime ngaySinh { get; set; }
        public int gioiTinh { get; set; }
        public int active { get; set; }
        public System.DateTime disableDate { get; set; }
        public int soDiem { get; set; }
        public int soLanNhanPhanThuong { get; set; }
        public Nullable<int> SuKien_idSuKien { get; set; }

        public virtual SuKien SuKien { get; set; }
        public virtual ICollection<CauHoiTinTuc> CauHoiTinTucs { get; set; }
        public virtual ICollection<NhanXetBanDoc> NhanXetBanDocs { get; set; }
        public virtual ICollection<QuyenThamGiaTroChoiTrongNgay> QuyenThamGiaTroChoiTrongNgays { get; set; }
        public virtual ICollection<Tin> Tins { get; set; }
    }
}