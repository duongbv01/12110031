﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TinTucWeb.Models
{
    public class NhanXetBanDoc
    {
        public NhanXetBanDoc()
        {
            this.NhanXetBanDocs1 = new HashSet<NhanXetBanDoc>();
        }
        [Key]
        public int idNhanXet { get; set; }
        public int idTin { get; set; }
        public string hoTen { get; set; }
        public System.DateTime ngayNX { get; set; }
        public string noiDungNX { get; set; }
        public string email { get; set; }
        public Nullable<int> NhanXetBanDoc_idNhanXet { get; set; }
        public Nullable<int> Account_idAccount { get; set; }

        public virtual Account Account { get; set; }
        public virtual ICollection<NhanXetBanDoc> NhanXetBanDocs1 { get; set; }
        public virtual NhanXetBanDoc NhanXetBanDoc1 { get; set; }
        public virtual Tin Tin { get; set; }
    }
}