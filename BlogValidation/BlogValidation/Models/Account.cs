﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BlogValidation.Models
{
    public class Account
    {
        //[Required]
        public int AccountID { set; get; }
        [Required]
        public string Password { set; get; }
        public string Email { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}