﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace BlogValidation.Models
{
    [Table("Bai Viet")]
    public class Post
    {
        [Range(2,100)]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public int ID { set; get; }
        [Required]
        public string Title {  set; get;  }
        [StringLength(250, ErrorMessage = "So ky tu trong khoang 5-250",MinimumLength=5)]
        public string Body {  set; get;  }
        [DataType(DataType.Date)]
        public DateTime DayCreated {  set; get;  }
        public DateTime DayUpdated { set; get; }
        public int AccountID { set; get; }
        public virtual Account Account { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }

    }
}
