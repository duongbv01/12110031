﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TinTucWeb.Models;

namespace TinTucWeb.Controllers
{
    public class CauHoiTinTucController : Controller
    {
        private TinTucDBContext db = new TinTucDBContext();

        //
        // GET: /CauHoiTinTuc/

        public ActionResult Index()
        {
            return View(db.CauHoiTinTucs.ToList());
        }

        //
        // GET: /CauHoiTinTuc/Details/5

        public ActionResult Details(int id = 0)
        {
            CauHoiTinTuc cauhoitintuc = db.CauHoiTinTucs.Find(id);
            if (cauhoitintuc == null)
            {
                return HttpNotFound();
            }
            return View(cauhoitintuc);
        }

        //
        // GET: /CauHoiTinTuc/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /CauHoiTinTuc/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CauHoiTinTuc cauhoitintuc)
        {
            if (ModelState.IsValid)
            {
                db.CauHoiTinTucs.Add(cauhoitintuc);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cauhoitintuc);
        }

        //
        // GET: /CauHoiTinTuc/Edit/5

        public ActionResult Edit(int id = 0)
        {
            CauHoiTinTuc cauhoitintuc = db.CauHoiTinTucs.Find(id);
            if (cauhoitintuc == null)
            {
                return HttpNotFound();
            }
            return View(cauhoitintuc);
        }

        //
        // POST: /CauHoiTinTuc/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CauHoiTinTuc cauhoitintuc)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cauhoitintuc).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cauhoitintuc);
        }

        //
        // GET: /CauHoiTinTuc/Delete/5

        public ActionResult Delete(int id = 0)
        {
            CauHoiTinTuc cauhoitintuc = db.CauHoiTinTucs.Find(id);
            if (cauhoitintuc == null)
            {
                return HttpNotFound();
            }
            return View(cauhoitintuc);
        }

        //
        // POST: /CauHoiTinTuc/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CauHoiTinTuc cauhoitintuc = db.CauHoiTinTucs.Find(id);
            db.CauHoiTinTucs.Remove(cauhoitintuc);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}