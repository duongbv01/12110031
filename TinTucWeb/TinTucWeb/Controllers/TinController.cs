﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TinTucWeb.Models;

namespace TinTucWeb.Controllers
{
    public class TinController : Controller
    {
        private TinTucDBContext db = new TinTucDBContext();

        //
        // GET: /Tin/

        public ActionResult Index()
        {
            var tins = db.Tins.Include(t => t.LoaiTin).Include(t => t.TheLoai);
            return View(tins.ToList());
        }

        //
        // GET: /Tin/Details/5

        public ActionResult Details(int id = 0)
        {
            Tin tin = db.Tins.Find(id);
            if (tin == null)
            {
                return HttpNotFound();
            }
            return View(tin);
        }

        //
        // GET: /Tin/Create

        public ActionResult Create()
        {
            ViewBag.idLoaiTin = new SelectList(db.LoaiTins, "idLoaiTin", "ten");
            ViewBag.idTheLoai = new SelectList(db.TheLoais, "idTheLoai", "tenTheLoai");
            return View();
        }

        //
        // POST: /Tin/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Tin tin)
        {
            if (ModelState.IsValid)
            {
                db.Tins.Add(tin);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idLoaiTin = new SelectList(db.LoaiTins, "idLoaiTin", "ten", tin.idLoaiTin);
            ViewBag.idTheLoai = new SelectList(db.TheLoais, "idTheLoai", "tenTheLoai", tin.idTheLoai);
            return View(tin);
        }

        //
        // GET: /Tin/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Tin tin = db.Tins.Find(id);
            if (tin == null)
            {
                return HttpNotFound();
            }
            ViewBag.idLoaiTin = new SelectList(db.LoaiTins, "idLoaiTin", "ten", tin.idLoaiTin);
            ViewBag.idTheLoai = new SelectList(db.TheLoais, "idTheLoai", "tenTheLoai", tin.idTheLoai);
            return View(tin);
        }

        //
        // POST: /Tin/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Tin tin)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tin).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idLoaiTin = new SelectList(db.LoaiTins, "idLoaiTin", "ten", tin.idLoaiTin);
            ViewBag.idTheLoai = new SelectList(db.TheLoais, "idTheLoai", "tenTheLoai", tin.idTheLoai);
            return View(tin);
        }

        //
        // GET: /Tin/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Tin tin = db.Tins.Find(id);
            if (tin == null)
            {
                return HttpNotFound();
            }
            return View(tin);
        }

        //
        // POST: /Tin/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tin tin = db.Tins.Find(id);
            db.Tins.Remove(tin);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}