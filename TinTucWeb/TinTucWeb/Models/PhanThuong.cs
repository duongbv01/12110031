﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TinTucWeb.Models
{
    public class PhanThuong
    {
        [Key]
        public int idPhanThuong { get; set; }
        public int idAccount { get; set; }
        public int tenPhanThuong { get; set; }
        public System.DateTime ngayNhan { get; set; }
    }
}