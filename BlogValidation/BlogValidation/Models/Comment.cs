﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BlogValidation.Models
{
    public class Comment
    {
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public int ID { set; get; }
        [StringLength(250, ErrorMessage = "So ky tu trong khoang 5-250", MinimumLength = 5)]
        public string Body { set; get; }
        public DateTime DayCreated { set; get; }
        public DateTime DayUpdate { set; get; }
        public int PostID { set; get; }
        public string Author { set; get; }
        public virtual Post Post { set; get; }

    }
}