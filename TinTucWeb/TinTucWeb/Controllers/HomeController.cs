﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TinTucWeb.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Chào mừng đến với website tin tức tổng hợp";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Trang thông tin about";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult TheGioi()
        {
            ViewBag.view = "Trang Tin Tức Thế Giới";
            return View();
        }
    }
}
