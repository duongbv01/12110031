﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TinTucWeb.Models
{
    public class LoaiTin
    {
        public LoaiTin()
        {
            this.Tins = new HashSet<Tin>();
        }
        [Key]
        public int idLoaiTin { get; set; }
        public string ten { get; set; }
        public string url { get; set; }
        public bool anHien { get; set; }
        public int idTheLoai { get; set; }
        public string target { get; set; }
        public string iconHinh { get; set; }

        public virtual ICollection<Tin> Tins { get; set; }
    }
}