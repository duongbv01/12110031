namespace BlogValidation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan5 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Bai Viet", "ID", c => c.Int(nullable: false));
            AlterColumn("dbo.Bai Viet", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Bai Viet", "Body", c => c.String(maxLength: 250));
            AlterColumn("dbo.Accounts", "Password", c => c.String(nullable: false));
            AlterColumn("dbo.Comments", "ID", c => c.Int(nullable: false));
            AlterColumn("dbo.Comments", "Body", c => c.String(maxLength: 250));
            AlterColumn("dbo.Tags", "Content", c => c.String(maxLength: 250));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Tags", "Content", c => c.String());
            AlterColumn("dbo.Comments", "Body", c => c.String());
            AlterColumn("dbo.Comments", "ID", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Accounts", "Password", c => c.String());
            AlterColumn("dbo.Bai Viet", "Body", c => c.String());
            AlterColumn("dbo.Bai Viet", "Title", c => c.String());
            AlterColumn("dbo.Bai Viet", "ID", c => c.Int(nullable: false, identity: true));
        }
    }
}
