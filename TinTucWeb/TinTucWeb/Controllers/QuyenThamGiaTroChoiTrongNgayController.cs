﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TinTucWeb.Models;

namespace TinTucWeb.Controllers
{
    public class QuyenThamGiaTroChoiTrongNgayController : Controller
    {
        private TinTucDBContext db = new TinTucDBContext();

        //
        // GET: /QuyenThamGiaTroChoiTrongNgay/

        public ActionResult Index()
        {
            return View(db.QuyenThamGiaTroChoiTrongNgays.ToList());
        }

        //
        // GET: /QuyenThamGiaTroChoiTrongNgay/Details/5

        public ActionResult Details(int id = 0)
        {
            QuyenThamGiaTroChoiTrongNgay quyenthamgiatrochoitrongngay = db.QuyenThamGiaTroChoiTrongNgays.Find(id);
            if (quyenthamgiatrochoitrongngay == null)
            {
                return HttpNotFound();
            }
            return View(quyenthamgiatrochoitrongngay);
        }

        //
        // GET: /QuyenThamGiaTroChoiTrongNgay/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /QuyenThamGiaTroChoiTrongNgay/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(QuyenThamGiaTroChoiTrongNgay quyenthamgiatrochoitrongngay)
        {
            if (ModelState.IsValid)
            {
                db.QuyenThamGiaTroChoiTrongNgays.Add(quyenthamgiatrochoitrongngay);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(quyenthamgiatrochoitrongngay);
        }

        //
        // GET: /QuyenThamGiaTroChoiTrongNgay/Edit/5

        public ActionResult Edit(int id = 0)
        {
            QuyenThamGiaTroChoiTrongNgay quyenthamgiatrochoitrongngay = db.QuyenThamGiaTroChoiTrongNgays.Find(id);
            if (quyenthamgiatrochoitrongngay == null)
            {
                return HttpNotFound();
            }
            return View(quyenthamgiatrochoitrongngay);
        }

        //
        // POST: /QuyenThamGiaTroChoiTrongNgay/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(QuyenThamGiaTroChoiTrongNgay quyenthamgiatrochoitrongngay)
        {
            if (ModelState.IsValid)
            {
                db.Entry(quyenthamgiatrochoitrongngay).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(quyenthamgiatrochoitrongngay);
        }

        //
        // GET: /QuyenThamGiaTroChoiTrongNgay/Delete/5

        public ActionResult Delete(int id = 0)
        {
            QuyenThamGiaTroChoiTrongNgay quyenthamgiatrochoitrongngay = db.QuyenThamGiaTroChoiTrongNgays.Find(id);
            if (quyenthamgiatrochoitrongngay == null)
            {
                return HttpNotFound();
            }
            return View(quyenthamgiatrochoitrongngay);
        }

        //
        // POST: /QuyenThamGiaTroChoiTrongNgay/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            QuyenThamGiaTroChoiTrongNgay quyenthamgiatrochoitrongngay = db.QuyenThamGiaTroChoiTrongNgays.Find(id);
            db.QuyenThamGiaTroChoiTrongNgays.Remove(quyenthamgiatrochoitrongngay);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}