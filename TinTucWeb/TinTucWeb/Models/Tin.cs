﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TinTucWeb.Models
{
    public class Tin
    {
        public Tin()
        {
            this.NhanXetBanDocs = new HashSet<NhanXetBanDoc>();
            this.Accounts = new HashSet<Account>();
        }
        [Key]
        public int idTin { get; set; }
        public string tieuDe { get; set; }
        public string tomTat { get; set; }
        public string urlHinh { get; set; }
        public System.DateTime ngayDangTin { get; set; }
        public int idAccount { get; set; }
        public int idSuKien { get; set; }
        public string content { get; set; }
        public int idLoaiTin { get; set; }
        public int idTheLoai { get; set; }
        public int trangThai { get; set; }

        public virtual LoaiTin LoaiTin { get; set; }
        public virtual ICollection<NhanXetBanDoc> NhanXetBanDocs { get; set; }
        public virtual TheLoai TheLoai { get; set; }
        public virtual ICollection<Account> Accounts { get; set; }
    }
}